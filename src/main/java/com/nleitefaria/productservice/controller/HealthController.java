package com.nleitefaria.productservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

    @GetMapping("product/service/health")
    public String getHealth(){
        return "OK";
    }

}
