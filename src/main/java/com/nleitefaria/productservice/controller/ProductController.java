package com.nleitefaria.productservice.controller;

import com.nleitefaria.productservice.domain.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ProductController {

    @GetMapping("product/details/{productId}")
    public Product getProductInfo(@PathVariable Long productId) {
        return new Product(101L, "productName", "productDesc", 999, true);

    }



}
